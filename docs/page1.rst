========================================================
THEJAS32 SoC Programmer's Manual
========================================================

Introduction
---------------

The THEJAS32 SoC (System-on-Chip) is a flexible and customizable platform designed for embedded systems. This programmer's manual aims to provide an overview of the architecture, instruction set, memory organization, programming model, and development tools for programming applications on an THEJAS32 SoC.

Architecture Overview
------------------------
The The THEJAS32 SoC is based on RISC-V RV32IM architecture of the RISC-V Instruction Set Architecture (ISA). It stands for RV32 (32-bit address space) and IM (integer multiplication and division). The architecture supports a set of instructions, including basic integer arithmetic, logical operations, load/store instructions, control flow instructions, and privileged instructions for system-level operations.

An THEJAS32 SoC typically consists of a ET1031 Processor core, memory subsystem, input/output interfaces, and various peripherals tailored for specific applications.

Features
~~~~~~~~

The THEJAS32 SoC provides the following features:

- 32-bit instruction and data formats
- 32 general-purpose registers (x0-x31)
- Machine Mode 
- Interrupt and exception handling
- System control and status registers (CSRs)
- Integer multiplication and division operations


Instruction Set Architecture
--------------------------------

The RV32IM architecture is a 32-bit RISC-V instruction set architecture that supports integer and multiplication/division operations. The architecture is divided into the following major components:

- Integer base instructions (RV32I)
- Integer multiplication and division extension (RV32M)

The THEJAS32 SoC supports a wide range of instructions, including but not limited to:

- Arithmetic and logical instructions (add, sub, and, or, xor, etc.)
- Load and store instructions (lw, sw, lb, sb, etc.)
- Branch and jump instructions (beq, bne, jal, jalr, etc.)
- Shift and comparison instructions (sll, srl, slt, etc.)
- Multiplication and division instructions (mul, div, etc.)

Refer to the RISC-V specification for a complete list of instructions supported by the RV32IM architecture.
