Memory Organization
----------------------
The memory organization in an THEJAS32 SoC typically consists of several memory regions, including instruction memory (often referred to as "text" segment), data memory (often referred to as "data" segment), stack memory, and peripheral memory regions.

The instruction memory stores executable code, and the data memory stores variables and data used by the program. The stack memory is used to store function call frames and local variables. The peripheral memory regions are used to interface with external devices and peripherals connected to the SoC.

The memory organization can vary depending on the specific THEJAS32 SoC implementation and the chosen memory map.

Memory Map
~~~~~~~~~~

The THEJAS32 SoC's memory map is as follows:

**ROM**

+------------------+---------------------------------+------------------------------------------+
|Memory Address    | Content                         | Size                                     |
+==================+=================================+==========================================+
|10000             |  Boot firmware                  | 32KB                                     |
+------------------+---------------------------------+------------------------------------------+
|17FFF             |  End of Memory                  |                                          |
+------------------+---------------------------------+------------------------------------------+

**IRAM**

+------------------+---------------------------------+------------------------------------------+
|Memory Address    | Content                         | Size                                     |
+==================+=================================+==========================================+
|20000             |  User defined firmware          | 96KB                                     |
+------------------+---------------------------------+------------------------------------------+
|37000             |  Data segment for firmware      | 32KB                                     |
+------------------+---------------------------------+------------------------------------------+
|3FFFF             |  End of Memory                  |                                          |
+------------------+---------------------------------+------------------------------------------+

**Flash**

+------------------+---------------------------------+------------------------------------------+
|Memory Address    | Content                         | Size                                     |
+==================+=================================+==========================================+
|000000            |  Reserved for Program Flashing  | 256KB                                    |
+------------------+---------------------------------+------------------------------------------+
|040000            |  User defined memory            | 1.75MB                                   |
+------------------+---------------------------------+------------------------------------------+
|200000            |  End of Memory                  |                                          |
+------------------+---------------------------------+------------------------------------------+




