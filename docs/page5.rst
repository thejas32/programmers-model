Programming Model
--------------------
The programming model for an RV32IM-based SoC follows the general principles of RISC-V programming. Programs are typically written in assembly language or compiled from high-level languages using a RISC-V toolchain.

To develop software for an THEJAS32 SoC, you need to understand the instruction set architecture, memory organization, and register usage. You also need to be familiar with the available development tools, such as assemblers, compilers, linkers, and debuggers, to build and debug your applications.

The specific programming model details, including calling conventions, stack usage, and system-level operations, may vary depending on the RV32IM implementation and the chosen software development environment.

Hello World Program
~~~~~~~~~~~~~~~~~~~
To create a "Hello, World!" C program to display text in a UART terminal on THEJAS32 SoC, you'll need a few components: the C code, the linker script (lds), the C runtime startup code (crt.S), and a Makefile to build the project. Below are the steps to achieve this:

1.Write the "Hello, World!" C program (main.c):

..  code-block:: cpp

	/*Function to print the string using UART Peripheral*/
	void print_string(const char *str)
	{
	    // Address of UART TX register
	    volatile char *tx = (volatile char *)0x10000100; 
	    // Address of UART LSR register
	    volatile char *lsr = (volatile char *)0x10000114;
	    while (*str) 
	    {
		*tx = *str; //Tranxmit a single char
		while ((*lsr& 0x20) != 0x20); //Check LSR for TX complete
		str++;
	    }
	}

	/*Main program*/
	void main() {
	    const char *message = " Hello, World!\n";
		
	    print_string(message);
		
		while(1);/*Infinite Loop*/
	}

2. Write the linker script (linker.lds):

..  code-block:: cpp
   
	OUTPUT_ARCH(riscv)
	ENTRY(_start)

	MEMORY
	{ 
	  ram (rwx) : ORIGIN = 0x200000, LENGTH = 250K
	}

	SECTIONS
	{
	  . = 0x200000;                   /*Start Address*/
	  .text.init : { *(.text.init) }  /*Init code*/
	  .text : { *(.text) }            /*Main Program*/
	  .data : { *(.data) }            /*Data Section*/
	  _end=.;                         /*End of section*/
	}


3. Write the C runtime startup code (crt.S):

..  code-block:: asm
    
	  .section ".text.init"
	  .globl _start

	  #init all registers with zero
	_start:
	  li  x1, 0
	  li  x2, 0
	  li  x3, 0
	  li  x4, 0
	  li  x5, 0
	  li  x6, 0
	  li  x7, 0
	  li  x8, 0
	  li  x9, 0
	  li  x10,0
	  li  x11,0
	  li  x12,0
	  li  x13,0
	  li  x14,0
	  li  x15,0
	  li  x16,0
	  li  x17,0
	  li  x18,0
	  li  x19,0
	  li  x20,0
	  li  x21,0
	  li  x22,0
	  li  x23,0
	  li  x24,0
	  li  x25,0
	  li  x26,0
	  li  x27,0
	  li  x28,0
	  li  x29,0
	  li  x30,0
	  li  x31,0

	#set the stack pointer as the end+1K
	  la  tp, _end 
	  add tp, tp, 1024
	  add sp, sp, tp

	#jump to main
	  j main

4. Write the Makefile (Makefile):

..  code-block:: Makefile
    
	CC=riscv64-vega-elf-gcc
	OC=riscv64-vega-elf-objcopy

	CFLAGS=-march=rv32im -mabi=ilp32 -Os -nostdlib -nostartfiles

	all: hello.elf

	hello.elf: main.c crt.S linker.lds
		$(CC) $(CFLAGS) -T linker.lds -o hello.elf crt.S main.c
		${OC} hello.elf hello.bin -O binary
	clean:
		rm -f hello.elf hello.bin	


5. Install any riscv toolchain that supports 'rv32im' extension, here we are using vega-tools(Prebuilt toolchain available in VEGA Gitlab repository)

..  code-block:: bash
     
	$ git clone https://gitlab.com/cdac-vega/vega-tools.git

6. Export the toolchain PATH in your terminal, or save it in your .bashrc file
	
..  code-block:: bash    

	$ export PATH=$PATH:/**<replace with your toolchain path>**/vega-tools/toolchain/bin/
	
7. Build the project:
Open a terminal in the directory containing the main.c, crt.s, linker.lds, and Makefile files. Then run the 'make' command to build the project:

..  code-block:: bash    
    
	$ make
	riscv64-vega-elf-gcc -march=rv32im -mabi=ilp32 -Os -nostdlib -nostartfiles -T linker.lds -o hello.elf crt.S main.c
	riscv64-vega-elf-objcopy hello.elf hello.bin -O binary
	
8. Transfer the 'hello.bin' file to any of the Aries Development boards using xmodem file transfer, Press Enter after file transfer, You can see the output in terminal

..  code-block:: bash   
    
	 +-----------------------------------------------------------------------------+                                   
	 |           VEGA Series of Microprocessors Developed By C-DAC, INDIA          |                                   
	 |     Microprocessor Development Programme, Funded by MeitY, Govt. of India   |                                   
	 +-----------------------------------------------------------------------------+                                   
	 | Bootloader, ver 1.0.0 [  (hdg@cdac_tvm) Tue Dec  15 16:50:32 IST 2020 #135] |                                   
	 |                                                                             |                                   
	 |  ___    _________________________          ISA  : RISC-V [RV32IM]           |                                   
	 |  __ |  / /__  ____/_  ____/__    |                                          |                                   
	 |  __ | / /__  __/  _  / __ __  /| |         CPU  : VEGA ET1031               |                                   
	 |  __ |/ / _  /___  / /_/ / _  ___ |                                          |                                   
	 |  _____/  /_____/  \____/  /_/  |_|         SoC  : THEJAS32                  |                                   
	 +---------------------------------------+-------------------------------------+                                   
	 |         www.vegaprocessors.in         |             vega@cdac.in            |                                   
	 +---------------------------------------+-------------------------------------+                                   
		                                                                                                           
	 Transfer mode  : UART XMODEM                                                                                      
		                                                                                                           
	 IRAM           : [0x200000 - 0x23E7FF] [250 KB]                                                                   
	 
	 Please send file using XMODEM and then press ENTER key.                                                           
	 CCCCCCCCC
	 Starting program ...




	 Hello, World!

.. note::
	1. To clean the project you can type 'make clean' command

	..  code-block:: bash   

		$ make clean
		rm -f hello.elf hello.bin
		
	2. Board may need to reset before you try to transfer a new program. You can see the 'CCC' is displayed on the terminal for the xmodem handshake.	





